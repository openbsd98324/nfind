
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <ctype.h>   // <- toupper on bsd and linux  


/*
char *strtouppercase( char *str )
{
	char *ptr = malloc( strlen(str)+1 );
	int j ; 
	int i;
	j = 0; 
	for(i=0; str[i]!='\0'; i++)
	{
		ptr[j++]=  toupper( str[i] ); 
	}
	ptr[j]='\0';
        size_t siz = sizeof ptr ; 
        char *r = malloc( sizeof ptr );
        return r ? memcpy(r, ptr, siz ) : NULL;
}
*/


char* strtouppercase(char* lower) 
{
	char* upper = malloc(strlen(lower)+1);
	char* p = upper;
	for (; *lower;) {
		*p++ = toupper(*lower++);
	}
	*p = '\0';
	return upper;
}




char searchitem[PATH_MAX];
void listdir(const char *name, int indent)
{
	//int i; int j ; 
	//char str1[PATH_MAX];
	//char str2[PATH_MAX];
	//char str[PATH_MAX];
	//char ptr[PATH_MAX];

	DIR *dir;
	struct dirent *entry;

    if (!(dir = opendir(name)))
        return;

    while ((entry = readdir(dir)) != NULL) 
    {
        if ( entry->d_type == DT_DIR ) 
	{
            char path[PATH_MAX];

            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;

            snprintf( path, sizeof(path), "%s/%s", name, entry->d_name);

            listdir( path, indent + 2);
        } 
	else 
	{
		   if ( strstr( strtouppercase(  entry->d_name )  ,  strtouppercase( searchitem  ) ) != 0 ) 
		   {
		          printf("%s/%s\n", name , entry->d_name );
		   }
        }
    }
    closedir(dir);
}







int main( int argc, char *argv[])
{
     if ( argc == 2 )
     {
         strncpy( searchitem, argv[ 1 ], PATH_MAX );          
         listdir( ".", 0 ) ;
     }
     return 0;
}




